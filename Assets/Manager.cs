﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {
    public static Manager instance = null;
    private int catalogSize = 100;
    [HideInInspector] public CatalogEntry[] catalog;
    public Player player;

	void Start () {
        if (!instance) instance = this;
        else Destroy(this.gameObject);

        catalog = new CatalogEntry[catalogSize];
	}
	
	void Update () {
		
	}
}

[System.Serializable]
public class CatalogEntry
{
    int id;
    float grade;
}

[System.Serializable]
public class AnimalConfig
{
    int id;
    float spawnChance;
    float hp;
}