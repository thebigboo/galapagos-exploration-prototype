﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    public Area currentArea;

    void Start () {
        SetArea(currentArea);
    }

    void OnAreaClicked(Area a)
    {
        if (currentArea.GetConnectedAreas().Contains(a))
        {
            currentArea.Leave();
            SetArea(a);
        }
    }

    void SetArea(Area a)
    {
        currentArea = a;
        transform.position = currentArea.transform.position;
        currentArea.SpawnContent();
    }

}
