﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animal : MonoBehaviour {

    private SpriteRenderer sr;
    public int type;
    public float spawnChanceOverride = 0f;

	void Start () {
        sr = GetComponent<SpriteRenderer>();
        //sr.sprite = Resources.Load<Sprite>("pokemon_3");
    }

    public void OnEnable()
    {
        TestSpawn();
    }

    public void TestSpawn()
    {
        float spawnChance;
        if (spawnChanceOverride > 0) spawnChance = spawnChanceOverride;
        else spawnChance = 1;
        if (Random.value > spawnChance)
        {
            this.gameObject.SetActive(false);
        }
    }
}
