﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObject : MonoBehaviour {
    public Transform target;
    public float smooth = 0.1f;

    void Start () {
		
	}
	
	void Update () {
        Vector2 TargetPos = Vector2.Lerp(transform.position, target.position, smooth);
        transform.position = new Vector3(TargetPos.x, TargetPos.y, transform.position.z);

    }
}
