﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Area : MonoBehaviour {
    public List<ConnectionMarker> connections;
    private int lastTurnVisited;
    private float lastTimeVisited;

    private void Awake()
    {
        if (connections.Count == 0)
        {
            connections = new List<ConnectionMarker>();
        }
    }

    void Start()
    {
        foreach (ConnectionMarker myConnection in connections)
        {
            bool retroConnection = false;
            foreach (ConnectionMarker theirConnection in myConnection.area.connections)
            {
                if (theirConnection.area == this)
                {
                    retroConnection = true;
                    break;
                }                
            }
            if (!retroConnection) myConnection.area.connections.Add(new ConnectionMarker(this, 1));
            CreateLine(transform.position, myConnection.area.transform.position);
        }
        DeactivateContent();
    }

    public List<Area> GetConnectedAreas()
    {
        List<Area> connectedAreas = new List<Area>();
        foreach (ConnectionMarker c in connections)
        {
            connectedAreas.Add(c.area);
        }

        return connectedAreas;
    }

    private void OnDrawGizmos()
    {
        if (connections.Count > 0)
        {
            foreach (ConnectionMarker c in connections)
            {
                if (c.area)
                {
                    Gizmos.color = Color.white;
                    Gizmos.DrawLine(transform.position, c.area.transform.position);
                }
            }
        }
        
    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Manager.instance.player.SendMessage("OnAreaClicked", this);
        }
    }

    public void SpawnContent()
    {
        Animal[] animals = GetComponentsInChildren<Animal>(true);
        foreach (Animal a in animals)
        {
            a.gameObject.SetActive(true);
        }
    }

    public void Leave()
    {
        DeactivateContent();
        lastTimeVisited = Time.time;
    }

    public void DeactivateContent()
    {
        Animal[] animals = GetComponentsInChildren<Animal>(true);
        foreach (Animal a in animals)
        {
            a.gameObject.SetActive(false);
        }
    }

    void CreateLine(Vector3 start, Vector3 end)
    {
        GameObject myLine = new GameObject();
        myLine.transform.position = start;
        myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        lr.startColor = Color.white;
        lr.startWidth = 0.1f;
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
    }
}

[System.Serializable]
public class ConnectionMarker
{
    public Area area;
    public int type;
    [HideInInspector] public Color color;
    [HideInInspector] public string text;

    public ConnectionMarker(Area a, int type)
    {
        area = a;
        color = Color.white;
        text = "hello";
    }
}